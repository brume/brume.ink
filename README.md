# brume.ink

Ce dépôt contient le code source du site [brume.ink](https://brume.ink), développé avec [Zola](https://www.getzola.org/), un générateur de sites statiques en Rust. 

Ce site regroupe mes illustrations et autres croquis. Il contient également un blog. 

Pour des raisons d'espace disque, la partie "Portfolio", qui contient les illustrations, n'est pas hébergée sur Gitea. 

## Consulter le site en local

1. Télécharger [Zola](https://www.getzola.org/documentation/getting-started/installation/)
2. `git clone https://git.42l.fr/brume/brume.ink.git && cd brume.ink`
3. `zola serve`
4. Ouvrir votre navigateur préféré (Firefox !) et visiter [localhost:1111](http://localhost:1111).

## Déployer le site

1. `zola build`
2. Déplacer le dossier en FTP sur le serveur
