+++
title = "Confections florales"
date = 2021-10-30
description = "Confection d'accessoires pour cheveux en tous genres, rubans de satin, nœuds printaniers, et kanzashi japonais."
[taxonomies]
categories = ["Tutoriels", "Artisanat"]
[extra]
month = "Oct"
illustration = "/blog/confections-florales/confections-florales.jpg"
+++

Depuis quelque temps déjà, je me passionne à la confection d'accessoires pour cheveux en tous genres. Des nœuds, des fleurs, des rubans... Je voulais vous présenter dans cet article mon cheminement, mes réalisations. Je ne rédigerai ici pas de tutoriel, par manque de temps et de moyens, mais je donnerai plusieurs vidéos montrant les techniques que j'ai pu utiliser. 

Depuis toujours, lorsque je rentre dans une mercerie, j'ai des étoiles dans les yeux. Dès alors, je m'imagine que coudre avec tous ces merveilleux rubans, galons ou boutons scintillants. Étant également intéressée par les accessoires pour cheveux, j'ai voulu mêler les deux. 

J'ai regardé beaucoup d'images sur internet pour trouver des inspirations, comprendre les différents styles existants. J'ai pu découvrir des styles de réalisation différents, en allant du simple nœud printannier en tissu coton imprimé, aux accessoires traditionnels japonais. Je vais présenter tout cela pas à pas. 

_Broche kanzashi composée de cinq fleurs et autres rubans_

{{ smaller_image(link="1.jpg",alt="Broche kanzashi composée de cinq fleurs et autres rubans") }}

## Les matériaux utilisés

Les drogueries et les boutiques de tissus sont des lieux qui m'ont toujours fascinée. Il y règne une ambiance particulière, assez indescriptible. Voir tous les rouleaux de tissus laisse place à l'imagination de tout ce que l'on aimerait coudre avec, des habits, accessoires, sacs, housses... Se laisser porter dans ce genre de boutique, généralement sur plusieurs étages, déambuler entre les cotons, les laines, les tulles ou encore les dentelles est un plaisir dont je ne saurais me passer. 

Fort heureusement, il existe à Paris un lieu tout destiné à cette activité : le [quartier du marché St-Pierre](https://fr.wikipedia.org/wiki/March%C3%A9_Saint-Pierre_(Paris)) (XVIIIe, près de Montmartre).

_Boutique Tissus Reine, Paris_

{{ smaller_image(link="2.jpg",alt="Boutique Reine, Paris") }}

J'y ai donc acheté quelques matériaux. Des cotons imprimés, de la doublure aux couleurs variées, des rubans de satin, des passepoils, des cordes... Pour les tissus, au vu de notre usage, des bandes de 50cm (le minimum dans la plupart des boutiques) suffisent largement. Cela reste donc assez économe. 

_Différents tissus : Liberty, écossais, chinois..._

{{ smaller_image(link="3.jpg",alt="Tissus divers") }}

Je me suis ensuite procuré un pistolet à colle bon marché sur internet. J'ai également commandé des pinces à cheveux, des broches, des rouleaux de plusieurs dizaines de mètres de rubans, des perles sur un site grossiste, [Pandahall](https://fr.pandahall.com/). Les prix étaient corrects, le matériel reçu est de qualité plus que convenable, le seul point négatif étant les longs délais de livraison.

_Rouleaux de plusieurs dizaines de mètres de rubans_

{{ smaller_image(link="4.jpg",alt="Rubans") }}

_Accessoires pour cheveux, pinces, piques, grelots, barrettes..._

{{ smaller_image(link="5.jpg",alt="Accessoires") }}
Avec ça, j'avais tout le matériel pour me lancer !

## Différents types de nœuds

### Le plus simple, les nœuds basiques en rubans

Ces nœuds ne demandent pas beaucoup de matériel, restent discrets mais mignons voire élégants. Ils sont également assez rapides à réaliser.
Pour cela, vous avez simplement besoin de rubans de satin, de couleur unie en général, de pinces à cheveux et d'un pistolet à colle. Il vous suffit de faire des boucles avec le ruban, de les coller ensemble, puis de recouvrir la pince de rubans, et de coller le nœud sur la pince. Vous pouvez tout à fait customiser votre nœud en rajoutant des greloques, des perles, plus de couleurs...

🎬 Voici un [tutoriel en vidéo](https://youtube.com/watch?v=AwWhhAdBXvA).

_Rubans divers rouges, bleus et verts_
{{ smaller_image(link="6.jpg",alt="Rubans divers rouges, bleus et verts") }}

### Un peu de couture avec les nœuds en tissu imprimé

Des envies printannières ? Ces nœuds sont faits pour vous. Pour les réaliser, vous aurez besoin de coudre un peu. Le type de tissu importe peu, le plus simple étant le coton imprimé. Même si cela reste plus aisé avec une machine à coudre, il est totalement possible de les concevoir à la main.

Le principe est simple : coudre un rectangle de tissu de taille variable, faire une boucle avec et coller au pistolet à colle.

🎬 Voici un [tutoriel en vidéo](https://youtube.com/watch?v=ORdMwxba_Vs).

_Nœuds printanniers_
{{ smaller_image(link="7.jpg",alt="Nœuds printanniers") }}

Il est également possible de mélanger les styles, voici par exemple un nœud avec à la fois du tissu et des rubans en satin. Celui-ci rend particulièrement bien !

_Nœud noir et rouge avec grelots_
{{ smaller_image(link="8.jpg",alt="Nœud noir et rouge avec grelots") }}

## Les kanzashi

Les kanzashi (簪) sont des ornements pour cheveux traditionnels japonais. Il en existe de tout type, des peignes, pinces, broches, épingles... et en toutes sortes de matières, bois laqué, or, argent, soie, nacre, pierres précieuses... Ils sont portés de manière variable en fonction de la classe sociale et du mois de l'année avec des tenues traditionnelles lors des mariages, cérémonies du thé ou encore par les geishas. Aujourd'hui, les kanzashi plus discret, moins élaborés peuvent même être portés comme accessoire de mode classique, au travail ou à l'école. 

Ce qui nous intéresse tout particulièrement ici, sont les hana kanzashi (花簪), les kanzashi fleuris. Ils sont portés par les apprenties geishas, les maiko, leurs coiffures étant beaucoup plus colorées et élaborées contrairement à celles des geishas, beaucoup plus sobres et strictes.

_Exemple de hana kanzashi porté par une maiko ([source de l'image](https://traditionalkyoto.com/culture/geiko-or-maiko/))_
{{ smaller_image(link="9.jpg",alt="Exemple de hana kanzashi porté par une maiko") }}

Chaque pétale est en réalité un carré de tissu fin dont la taille varie entre 2 et 5 centimètres, confectionné selon un pliage particulier. Les pétales sont ensuite assemblés, soit à la colle, soit cousus, pour former de petites fleurs. Il est ensuite possible de rajouter des grelots, des rubans, des petites feuilles...

🎬 Voici un [tutoriel en vidéo](https://youtube.com/watch?v=rICe8L08xVQ).


_Photo prise en pleine réalisation, avec les pétales et fleurs non assemblés et les carrés de tissus_
{{ smaller_image(link="10.jpg",alt="Photo prise en pleine réalisation, avec les pétales et fleurs non assemblés et les carrés de tissus") }}

_Divers kanzashi en épingle, broche, barrettes..._
{{ smaller_image(link="11.jpg",alt="Divers kanzashi") }}

## Et ensuite ?

Réaliser des nœuds divers me plaît bien, et je porte à présent quotidiennement mes créations. Elles font partie intégrante de mon style vestimentaire. J'aimerais, à l'avenir, m'améliorer dans le domaine. Cela passe par composer de nouvelles créations, rendre les finitions impeccables, bannir les traces de colle, fils qui dépassent et autres joyeusetés. Je constate aussi que certaines pièces sont assez fragiles. De plus, elles me prennent toujours beaucoup de temps à réaliser. J'ai donc encore de nombreux points sur lesquels progresser.

Les réaliser me prend certes un certain temps mais j'aimerais, un jour, pouvoir les vendre en ligne. Si cela arrive, je ne me vois pas utiliser un service tiers de vente tel qu'Etsy ou autre, je me débrouillerai plutôt pour avoir ma propre boutique en ligne sur mon site. Forcément, cela sera plus long à déployer, et cela aura un impact sur la visibilité, mais il me semble beaucoup plus plaisant d'utiliser un service auto-hébergé. 

Si cela vous intéresse, j'ouvre une [catégorie du portfolio](/portfolio/#Artisanat) consacrée à ces réalisations ! J'y posterai les derniers nœuds en date. Si ces créations vous plaisent, avant même qu'une potentielle boutique soit créée, n'hésitez pas à me contacter, je devrais être en mesure de faire des commandes sur mesure !

Merci de votre lecture, 

Brume
