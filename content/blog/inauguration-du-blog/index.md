+++
title = "Inauguration du blog !"
date = 2020-04-12
description = "Présentation du site et de ce qui y sera publié."
[taxonomies]
categories = ["Divers"]
[extra]
month = "AVR"
illustration = "/blog/inauguration-du-blog/brume-site.jpg"
+++

## Bonjour ! 

Je me présente, Brume. Je suis étudiante à l'école 42, une école d'informatique dans le XVIIe, à Paris. En parallèle à mes études, je dessine : dans les transports, pour un livre ou encore pour mon association, 42l. 

J'ai créé ce site pour vous présenter un peu mon travail. L'envie d'exposer mes dessins en ligne se faisait ressentir depuis déjà quelques mois mais je n'osais pas trop me lancer, par manque de temps principalement. La sortie du [livre](/projets/tsa-ted-tdah-ce-yoga-est-pour-vous) que j'ai illustré m'a donné une occasion de concrétiser cette idée.

Il est séparé en trois grandes parties : 
- Les [projets](/projets) : une liste de pages consacrées à des gros projets auxquels j'ai pu prendre partie. Ces pages permettent de présenter toutes les parties du travail, d'un point de vue graphique : le logo, l'identité graphique... Le format est très libre, ce qui me permet réellement de présenter la réalisation sous les angles qui m'intéressent et de mettre autant de contenu que nécéssaire. Pour l'instant, il y a deux projets : l'association [42l](/projets/42l) et le livre [TSA-TED-TDAH ce yoga est pour vous](/projets/tsa-ted-tdah-ce-yoga-est-pour-vous). 

- Le [portfolio](/portfolio#Croquis) : des galleries présentant une vue d'ensemble des croquis, illustrations et logo que j'ai pu réaliser. Chaque série d'images, ou image seule si ce sont des travaux isolés, possède une page consacrée, qui décrit grossièrement le travail réalisé.

- Le [blog](/blog) : il sera plus utilisé pour témoigner de mes états d'esprit, mes passions vagabondes, mes émotions. Il n'y a pas de rythme de parution des articles fixé, cela sera quand j'en aurai l'envie, principalement. Les articles peuvent être triés par catégories. 

L'intégralité de ce site a été réalisé avec [Zola](https://www.getzola.org/), un générateur de sites statiques. Si cela vous intéresse, j'en parle un petit peu plus [ici](/a-propos). 

J'ai pensé sérieusement au site et au nom de domaine à partir de Décembre 2019. J'ai débuté lentement à le réaliser courant Janvier, mais faute de temps disponible, je l'ai laissé de côté jusqu'à Mars. La situation actuelle me permet d'avoir beaucoup plus de temps. J'ai donc pu finalement me concentrer totalement à la réalisation de ce site. Mis bout-à-bout, je dirais donc qu'il y a environ un mois de travail dessus. 

Le plus difficile dans la conception de ce site internet a été la prise en main de Zola. Au début, je ne savais jamais comment faire quoi que ce soit, j'étais en permanence bloquée et tout me semblait très obscur. J'ai eu un déclic, lorsque j'ai commencé à y consacrer plus de temps. La maîtrise de Zola m'a vraiment permis d'avancer plus vite, et d'être plus sûre dans ce que je faisais. Une fois compris, ce n'est pas si compliqué à utiliser ! Je songe à faire un tutoriel dans les semaines qui suivent. D'une part, pour m'en souvenir et tout retrouver rapidement, mais également, pour permettre à des gens qui le souhaiteraient d'arriver à l'utiliser rapidement sans lutter comme je viens de le faire.

J'espère, au travers de ce site, pouvoir vous faire voyager, vous faire découvrir mon univers graphique. J'essaierais de poster des nouveaux dessins et des articles de blog au fur et à mesure. Un <a href="/rss.xml" download>flux RSS</a> est disponible si vous souhaitez être au courant des publications. Merci de votre lecture et à bientôt ! 

Brume
