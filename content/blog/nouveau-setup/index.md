+++
title = "Nouveau PC, nouveau setup !"
date = 2023-02-23
description = "Présentation de mon nouvel ordinateur portable, un Tuxedo XXX, et de son setup sous Arch !"
[taxonomies]
categories = ["Informatique"]
[extra]
month = "FÉV"
illustration = "/blog/nouveau-setup/keyboard-1.png"
+++

Mon ancien ordinateur datant de 2014, il commençait à se faire vieux. Bien qu'aucun composant n'était défectueux, ses capacités et surtout ses 4Go de RAM n'étaient plus suffisants pour faire tourner ne serait-ce que Krita sans crasher à intervalles réguliers. J'ai donc pris la décision de changer d'ordinateur, à contre-cœur, car je m'y étais beaucoup attachée ces dernières années. Je me suis donc tournée vers un [Tuxedo](https://www.tuxedocomputers.com/). Changer d'ordinateur était aussi pour moi l'occasion de changer radicalement de setup : je passe de Debian à Arch, avec Sway en environnement de bureau. Dans cet article, je vais donc vous raconter ce long périple...

{{ banner_image(link="keyboard-1.png",alt="Photo du clavier") }}

## Le matériel

### Spécifications

Il s'agit du modèle [TUXEDO Aura 15 - Gen2](https://www.tuxedocomputers.com/en/Linux-Hardware/Notebooks/15-16-inch/TUXEDO-Aura-15-Gen2.tuxedo). Voici sa configuration :
- CPU : AMD Ryzen 7 5700U ;
- RAM : 16 GB ;
- Stockage : 1 SSD de 1TB.

J'avais longuement hésité entre les Tuxedo, les [Framework](https://frame.work/fr/fr) et les [Starlabs](https://fr.starlabs.systems/). Je voulais un PC plutôt léger et peu encombrant, avec une préférence pour un châssis métallique. Ah, et aussi, de l'AMD, ainsi qu'une bonne compatibilité Linux. Cela commençait à faire beaucoup de critères, que je n'ai pas complètement pu satisfaire. La particularité très intéressante de la marque Tuxedo, celle qui m'a permis de me décider, réside dans leurs claviers : il est possible d'obtenir un clavier personnalisé en leur envoyant un SVG, qui sera gravé sur l'ordinateur.

### Clavier

Je suis plutôt du genre enthousiaste à customiser le moindre détail, alors bien évidemment : j'ai entièrement réalisé mon clavier en vectoriel, jusqu'à la moindre icône. J'ai un peu craqué, j'ai mis des petits détails partout :

{{ medium_image(link="keyboard.png",alt="Layout du clavier QWERTY, très personnalisé") }}

On remarquera, dans le tas, des fleurs de cerisier, des feuilles de ginko, des Totoro, un chat, un papillon, des étoiles, des petites vagues, le logo de La Contre-Voie... Le clavier japonais reprend le [standard JIS たていすかん](https://ja.wikipedia.org/wiki/JIS%E3%82%AD%E3%83%BC%E3%83%9C%E3%83%BC%E3%83%89). Bien qu'il s'agisse d'un vrai clavier, il est ici très cosmétique, je suis plutôt habituée à taper les kanas avec le QWERTY. 

La police utilisée pour les caractères romains est [Primer](https://typodermicfonts.com/primer/). Elle est libre de droit pour l'utilisation personnelle et commerciale. Je recherchais une police assez ronde et enfantine pour aller avec le reste du design. La police des caractères japonais est [Noto Sans JP](https://fonts.google.com/noto/specimen/Noto+Sans+JP), une police Google très classique sous [Open Font License](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL).

J'ai pris un malin plaisir à personnaliser le clavier le plus possible, j'ai par exemple rajouté les accents français les plus communs sur les touches de mon choix, des petites flèches sur les touches HJKL (raccourcis de Vim), ou encore le point médian sur la touche M.

Et voilà le résultat final :

{{ medium_image(link="keyboard-2.png",alt="Photo du clavier") }}
{{ medium_image(link="keyboard-3.png",alt="Photo du clavier") }}

### Avis général

Bien qu'ayant reçu ce nouvel ordinateur fin août 2022, je n'ai fait le transfert qu'en décembre. C'est en partie lié au fait que j'étais un peu trop débordée pour m'en occuper, mais aussi parce que j'avais beaucoup de mal à lâcher mon ancien PC, que j'affectionnais tout particulièrement. J'ai eu du mal à m'approprier cet ordinateur et à l'apprécier. Ce n'est pas encore tout à fait le cas, mais ça vient. 🙂

Cela fait donc très peu de temps que je l'utilise. Pour l'instant, j'en suis satisfaite. Il n'y a aucun problème majeur ou dysfonctionnement particulier. Je crois que la caméra n'est pas excellente, mais cela s'arrête là. Je n'ai pas vraiment eu l'occasion de le pousser dans ses retranchements non plus ; à part Krita et quelques machines virtuelles, je n'ai rien tenté d'extravagant non plus !

## Le setup

### Installation
L'ordinateur maintenant présenté, nous allons pouvoir nous attaquer au vif du sujet : l'installation ! J'ai fait le choix de passer de Debian à Arch, car je voulais apprendre un peu plus à gérer mon système et mon infrastructure. Je voulais pouvoir tout customiser au maximum pour m'approprier au mieux ce nouveau PC. _Et puis, je me disais qu'il fallait bien faire une installation de Arch au moins une fois dans sa vie !_

L'installation s'est très bien passée, je n'ai eu aucun souci majeur. Il m'a suffi de suivre [le guide sur le wiki de Arch](https://wiki.archlinux.org/title/Installation_guide), puis de naviguer dans [la liste des applications](https://wiki.archlinux.org/title/List_of_applications).

### Btrfs

Pour bien m'embêter encore plus, j'ai choisi [Btrfs](https://wiki.archlinux.org/title/Btrfs) en filesystem (le système de stockage de fichiers). Btrfs ne supportant pas le chiffrement par défaut, j'ai dû faire une installation un peu custom avec LUKS.

Voici quelques liens sur Btrfs :
- [btrfs](https://sebsauvage.net/wiki/doku.php?id=btrfs) ;
- [Installing Arch Linux with Btrfs, systemd-boot and LUKS](https://nerdstuff.org/posts/2020/2020-004_arch_linux_luks_btrfs_systemd-boot/) ;
- [LUKS Root on Btrfs](https://wiki.archlinux.org/title/User:M0p/LUKS_Root_on_Btrfs).

### Sway

En serveur d'affichage, ou _display manager_, je suis partie sur Wayland, avec XWayland (module de support de X11). Comme explicité plus haut, j'ai choisi gestionnaire de fenêtres [Sway](https://wiki.archlinux.org/title/Sway). Son fonctionnement ressemble un peu à celui de [tmux](https://github.com/tmux/tmux/wiki) : il est possible de changer de fenêtres uniquement via des raccourcis clavier. Il est évident que comparé à Gnome, cela est assez déroutant ! La courbe de progression est assez élevée mais une fois maîtrisé, c'est plutôt agréable.

Toute la configuration de Sway se retrouve dans le dossier `~/.config/sway`, et en particulier le fichier `~/.config/sway/config`. C'est à l'intérieur de ce fichier que nous allons pouvoir préciser chaque raccourci utilisé, la gestion des entrées/sorties (claviers, écrans...).

Il est également possible d'entièrement personnaliser sa barre des tâches, en haut de l'écran. Pour cela j'utilise [Waybar](https://github.com/Alexays/Waybar/wiki). J'estime ne pas avoir du tout terminé de personnaliser cette partie, mais voilà à quoi elle ressemble :

{{ banner_image(link="waybar.png",alt="Ma barre de tâches") }}

Chaque module est complètement configurable, et la barre est stylisée en CSS. Mes fichiers de configuration de sway et de waybar sont respectivement disponibles [ici](https://git.42l.fr/brume/dotfiles/src/branch/main/sway/config) et [ici](https://git.42l.fr/brume/dotfiles/src/branch/main/waybar/config).

J'ai aussi écrit quelques scripts de personnalisation, pour afficher la musique en cours dans ma barre des tâches (disponible [ici](https://git.42l.fr/brume/dotfiles/src/branch/main/waybar/scripts/music.sh)) par exemple. Même si ce n'est pas parfait, c'est vraiment plaisant de pouvoir complètement tout configurer !

Quelques liens sur Sway :
- [Sway Wiki](https://github.com/swaywm/sway/wiki)
- [Useful add ons for sway](https://github.com/swaywm/sway/wiki/Useful-add-ons-for-sway)
- [Waybar Examples](https://github.com/Alexays/Waybar/wiki/Examples)

Pour la mise en veille, j'utilisais `sway-idle` et [swaylock-effects](https://github.com/mortie/swaylock-effects). Je m'étais fait un joli écran de veille très personnalisé, malheureusement, depuis une récente mise à jour il n'affiche plus qu'un écran tout rouge impossible à retirer... C'est aussi ça les aléas de Arch, je suppose. En attendant de trouver mieux, je me suis rabattue sur la version classique de [swaylock](https://github.com/swaywm/swaylock).


### Ulauncher

J'utilise [Ulauncher](https://ulauncher.io/) comme lanceur d'applications. J'avais un peu regardé les autres alternatives et hésité avec d'autres, mais celui-là m'a attirée avec son style plutôt propre. Et puis, peut-être que leur site aux allures de startuppeur m'a rassurée quand j'arrivais tout juste sur Arch ! 😁

Avec un simple raccourci (`Super + D`), cette barre apparaît :

{{ medium_image(link="ulauncher.png",alt="Ulauncher") }}

Il est plutôt bien modulable, et propose quelques extensions sympathiques, comme pouvoir copier coller un emoji très facilement ou faire de la recherche de fichiers avec `fzf`.


### Dunst

Je me suis également pas mal amusée avec [Dunst](https://wiki.archlinux.org/title/Dunst), le logiciel que j'utilise en serveur de notifications. Bien qu'assez correctes, j'estime mes notifications pas encore parfaites, mais [voici leur configuration](https://git.42l.fr/brume/dotfiles/src/branch/main/dunst/dunstrc).

{{ medium_image(link="notif-1.png",alt="Une notification basique") }}

Je me suis également appliquée à créer des notifications de batterie faible, de changement de volume et autres joyeusetés personnalisées, en les combinant avec les options de Sway et de Waybar. C'était plutôt sympathique à faire !

{{ medium_image(link="notif-2.png",alt="Une notification de batterie et une de volume.") }}


### Paquets installés

La liste de mes paquets installés se trouve [ici](https://git.42l.fr/brume/dotfiles/src/branch/main/installed-packages.txt). Rien de particulièrement sorcier. Globalement, j'utilise :

- iwd pour le wifi ;
- PulseAudio pour le son ;
- brightnessctl pour la luminosité ;
- paru pour la gestion des paquets ;
- grimshot pour les captures d'écran ;
- evince pour les PDFs ;
- imv pour les images ;
- vlc pour les vidéos ;

J'en profite pour faire une petite mention à `nvlc`, une version en ligne de commande de VLC pour écouter de la musique, utilisant `ncurses`. J'adore son interface au point de ne pas écouter de musique sans !

{{ medium_image(link="nvlc.png",alt="NVLC") }}

Pour les applications de bureau :
- Firefox et Thunderbird ;
- LibreOffice ;
- GIMP, Krita, Inkscape ;
- KeepassXC...

N'étant pas satisfaite d'Inkscape, j'utilise beaucoup Illustrator dès qu'il s'agit du vectoriel. Cependant, Adobe et Linux ne font pas très bon ménage. Actuellement, j'utilise une vieille version à l'aide de [PlayOnLinux](https://en.wikipedia.org/wiki/PlayOnLinux), une surcouche graphique de Wine. C'est lent, c'est moche, c'est un peu cassé de partout, mais ça fonctionne. 

Mon explorateur de fichiers est [Nemo](https://wiki.archlinux.org/title/Nemo), un fork de Nautilus, l'explorateur de fichiers de Gnome.

Pour ma tablette graphique, j'ai testé au moins trois drivers libres avant d'abandonner et d'installer le driver du fabricant, le paquet `xp-pen-tablet`. Certains drivers marchaient, mais comme j'utilise le mode gaucher de la tablette (ce qui implique notamment de la retourner et d'avoir un petit décalage sur là où appuie le stylet), c'est un peu délicat. J'ai dû aussi configurer les options de sortie de Sway pour gérer l'écran de la tablette. Je n'ai pas eu d'autres soucis et arrive bien à utiliser ma tablette graphique avec Krita !

### Clavier

J'ai utilisé [XKB](https://wiki.archlinux.org/title/X_keyboard_extension), pour me créer mon propre layout de clavier (disponible [ici](https://git.42l.fr/brume/dotfiles/src/branch/main/xkb/symbols/us-modified)), en adéquation avec les touches custom imprimées sur le clavier. J'utilise le QWERTY, mais les accents, le point médian et des caractères spéciaux ont été ajoutés. C'est assez commode de pouvoir mapper chaque caractère de son choix avec XKB, et d'ajouter autant de caractères spéciaux que l'on veut avec les combinaisons de touches de son choix.

Une grosse difficulté à surmonter fut le clavier japonais. Car même si un clavier たていすかん est imprimé sur mon clavier, j'utilise plutôt le QWERTY, qui doit être directement converti en kana ou en kanjis. Ça a été assez complexe à mettre en place. J'ai testé toutes les solutions de [cette page](https://wiki.archlinux.org/title/Input_Japanese_using_uim) avant d'explorer du côté de [Fcitx](https://wiki.archlinux.org/title/Fcitx). [Cette vidéo](https://piped.video/watch?v=x8h2Maou21o) en japonais, bien qu'il y ait beaucoup de superflu, m'a été utile pour m'en sortir. Et puis, l'accent du monsieur est fantastique ! リブートしまあああす!

Pour la gestion des ventilateurs et du clavier rétro-éclairé, j'utilise [tuxedo-rs](https://github.com/AaronErhardt/tuxedo-rs). Par contre, je n'ai pas encore eu le loisir de vraiment paramétrer les couleurs donc pour l'instant, il boucle en arc-en-ciel. Heureusement que je peux au moins de désactiver !

### Terminal

Mon terminal est [Alacritty](https://wiki.archlinux.org/title/Alacritty). Je n'ai pas vraiment exploré les autres possibilités. J'ai directement installé [fish](https://fishshell.com/), ma shell de prédilection, ainsi que tmux. On rajoute à cela Vim, et c'est bon, j'ai mon environnement de travail !

Plus sérieusement, j'affectionne vraiment fish pour ses fonctionnalités d'autocomplétion et d'historique. Il est difficile de revenir sur d'autres shells après l'avoir testé. Et pour tmux, c'est aussi réellement utile. Fini les mille et une fenêtres de terminaux éparpillées, tout est maintenant dans une seule fenêtre, séparé et rangé proprement. Enfin, Vim est mon éditeur de texte par défaut. Cet article a été écrit dans Vim ! Actuellement, j'utilise le thème [Sonokai](https://github.com/sainnhe/sonokai).

## Pour finir

Même si je vais mettre du temps à complètement apprécier mon PC et à me l'approprier, je suis déjà assez satisfaite de mon setup actuel. Il me manque certainement encore quelques éléments cruciaux (je n'ai pas installé CUPS...), mais je peux déjà travailler et utiliser ce nouvel ordinateur dans la vie de tous les jours.

Il y a plein d'éléments que je considère comme inachevés (ma barre des tâches, mon écran de veille...), mais je suppose qu'on estime n'avoir jamais réellement terminé son setup, qu'on est toujours un petit peu insatisfait, quelque part.

Concernant Arch, je suis plutôt fière d'avoir sauté le pas. Je ne trouve finalement pas la marche si haute en comparaison avec Debian. Mais au moins j'ai l'impression d'un peu plus maîtriser mon PC et ce qu'il y a dessus, c'est pas mal ! Pour Sway, je suis très contente. Même si on part de rien, et qu'il n'y a rien d'intégré pour nous faciliter la vie comme Gnome où il y aura toujours toutes les options dont on n'a même pas encore rêvé, c'est assez agréable d'avoir une base minimaliste et d'installer que ce dont nous avons besoin.

Certain·es apprécient beaucoup Arch parce que c'est une _rolling release_, les paquets sont toujours le plus à jour possible. Il arrive parfois que certains paquets cassent (comme c'est le cas actuellement pour moi avec `swaylock-effects`), mais il y a souvent des solutions alternatives, on arrive à s'en sortir. Je n'ai pas spécialement choisi Arch pour cette raison, n'étant pas spécialement une fanatique des mises à jour. Pour moi, c'est plus pour les possibilités de customisation et de personnalisation. C'est à travers ce domaine que j'arrive à m'approprier mon matériel.

Merci d'avoir lu jusqu'ici !

Brume
