+++
title = "Un peu de piano"
date = 2020-11-17
description = "Présentation de mon parcours en piano."
[taxonomies]
categories = ["Musique"]
[extra]
month = "NOV"
illustration = "/blog/un-peu-de-piano/un-peu-de-piano.jpg"
+++

En mai 2020, j'ai commencé à me remettre à jouer du piano, et ce de manière intensive. 

Tout a commencé lorsque, dans notre appartement, le piano servant de repose-tout, de bibliothèque, a été déplacé dans ma chambre. Désencombré de tous ses papiers et autres babioles, je l'ai ouvert, dépoussiéré, et... redécouvert. 

_Attention : les extraits musicaux présents dans cet article proviennent du site [imslp.org](https://imslp.org/). Ils ne sont pas de moi !_

## Découverte de la musique

Étant enfant, je jouais déjà du piano. J'en jouais correctement, mais je n'étais pas une élève assidue. Je ne travaillais pas entre les leçons. J'ai eu, en tout, plusieurs professeurs mais surtout une brillante, patiente et adorable. J'avais commencé une méthode seule, avec des morceaux simples, comme Au clair de la lune, J'ai du bon tabac... Je me suis petit à petit, avec cette professeure, décrochée de cette méthode pour entrer dans la musique classique.

Mon premier vrai morceau a été le premier _Prélude de Bach en Do Majeur (BVW 846)_. Je ne savais même pas vraiment lire les partitions à ce moment, ma professeure avait écrit toutes les notes du morceau sur mon cahier (cette phrase fera hurler tous·tes les musicien·nes !). C'est un morceau que j'ai énormément joué, que je joue encore souvent, et qui a été pendant longtemps, le premier morceau que je jouais en voyant un piano. Je trouve que toute sa beauté réside dans la manière dont est jouée chaque mesure, la force mise dans les doigts pour jouer un arpège dissonant suite à un arpège parfait léger. 

{{ mp3(link="Bach-BVW-846.mp3") }}

J'ai ensuite joué _L'Hirondelle_, un morceau de Burguller, un compositeur allemand moins connu. Je trouve ses morceaux très imagés, fleuris. Il y a des fleurs, des animaux, des oiseaux qui volent quand on les entend. J'ai aussi joué la _Gymnopédie No.1 de Satie_ et découvert cet univers monotone, empli de langueur et de nostalgie. Il est intéressant de voir que chaque compositeur ont leur univers précis et reconnaissable. 

Les trop connues _Lettre à Élise_ et _Marche Turque_ sont venues ensuite. J'aimais les morceaux phares, connus. C'est souvent uniquement les thèmes qui sont joués. Cela m'a pris pas mal de temps, mais j'ai décidé les jouer en entier, malgré la difficulté. Jouer les morceaux à la perfection, sans aucune fausse note ne m'intéresse pas vraiment. Je préfère jouer les morceaux que j'aime, même s'ils ne seront pas parfaits.  

Je ne lisais pas très bien. J'ai tendance, encore maintenant, à retenir les morceaux par cœur, et de me détacher de la partition. Cet apprentissage était souvent instantané : je voyais les morceaux comme des suites logiques, des formes géométriques, des lignes. S'il est pas évident de retenir des séries de notes détachées, des images l'est beaucoup plus ! Chose assez étonnante, d'ailleurs, car si ma mémoire est excellente pour retenir des partitions, elle est catastrophique pour retenir une musique entendue. 

Ensuite, au lycée et à mes premières années à 42, j'ai presque totalement arrêté de jouer, par manque de temps, et par manque d'accès à un piano. Cela me manquait un peu, je me disais souvent que c'était dommage, mais je ne pouvais pas y faire grand-chose à ce moment. J'ai tout de même appris la _Valse de Chopin en Si Mineur (Op. posth 69 No.2)_.

{{ mp3(link="Chopin-69-2.mp3") }}

## Conservatoire ?

À l'inverse des cours particuliers, j'ai aussi eu l'occasion d'étudier deux ans au conservatoire. Il est très difficile de passer de l'un à l'autre, les méthodes sont très différentes. Un cours de solfège à part est obligatoire, d'une durée d'une heure trente par semaine, tandis que le cours d'instrument, dure une demi-heure. Le conservatoire permet des bases solides en solfège et en pratique de la musique, mais il a tendance à écœurer les enfants par sa rigueur et sa rigidité. À partir d'un certain âge, c'est souvent tourné uniquement autour de la performance et de la compétition. Nombre d'enfants ont été dégoûtés de la musique pour ça.

Pour les cours particuliers, cela dépendra énormément du professeur. Généralement, le solfège est assez délaissé. Les cours sont plus longs, en général, une heure. Contrairement au conservatoire, ou le cours sert à vérifier le travail de la semaine et donner des indications, le cours particulier sert vraiment à travailler et accompagner l'élève.

Point supplémentaire, il est très difficile de commencer dans un conservatoire à l'âge adulte : les cours de solfège sont classés par niveau/grade, donc les adolescent·es/adultes débutant·es sont placé·es... avec les enfants de 5 ans. Être seule dans un cours avec des enfants qui ne savent qu'à peine écrire, je vous assure que ce n'est pas passionnant. Il est en revanche possible de commencer des cours particuliers à n'importe quel âge, même à la retraite !

Voici le sixième _Prélude de Bach en Ré Mineur (BVW 851)_, que j'ai eu l'occasion de travailler récemment.
{{ mp3(link="Bach-BVW-851.mp3") }}

## Et maintenant ? 

Pendant le confinement, n'ayant plus de contrainte de temps, je jouais trois à quatre heures par jours. Maintenant, avec la rentrée, je tourne plutôt autour d'une heure. J'ai repris, retravaillé de zéro, tous les morceaux que je connaissais. Ça allait assez vite, car les doigts se souviennent très bien des mouvements, au point qu'il est plus facile de laisser ses doigts jouer que d'essayer de se souvenir des notes. 

J'ai ensuite joué de nouveaux morceaux : 
- Le premier mouvement de _La Sonate au Clair de Lune de Beethoven (Op.27 No.2)_, assez facile,

- Le second _Prélude de Bach en Do Mineur (BVW 847)_, mon coup de cœur (le nouveau premier morceau que je joue tout le temps) qui est assez impressionnant,

- Le sixième _Prélude de Bach en Ré Mineur (BVW 851)_, présenté plus haut,

- Des _Préludes de Chopin (Op. 28)_,

- Le premier mouvement de la _Sonate K.545 en Do Majeur de Mozart (No.16)_. 

Ci-dessous se trouvent dans l'ordre, le second _Prélude de Bach_ et la _Sonate K.545 de Mozart_. 
{{ mp3(link="Bach-BVW-847.mp3") }}
{{ mp3(link="Mozart-K-545.mp3") }}

En ce moment, je travaille la _Sonate D.845 de en La Mineur Schubert (Op.42 No.16)_, que je trouve magnifique. C'est un morceau assez dur et long, j'ai été assez ambitieuse en le choisissant. Je l'ai commencé depuis environ 2 mois et je l'ai terminée, mais je ne la joue pas encore parfaitement. 

{{ mp3(link="Schubert-D-845.mp3") }}

Faire plein de morceaux différents, enrichir son répertoire, c'est aussi découvrir l'univers des compositeurs : discerner leurs habitudes, leur style, arriver à le reconnaître... Des œuvres tragiques, empathiques, imposantes de Beethoven aux légères et frétillantes œuvres de Mozart, en passant par le style imagé, lunaire de Debussy.

Je ne pourrais probablement pas me permettre de jouer une heure de piano par jour pendant longtemps, mais c'est ce qui me fait vivre en ce moment. Beaucoup de mes occupations sont orientées sur la musique : écoute et recherche de morceaux et de partitions, lecture/visionnage de livres/films sur le sujet, recherches de sites internet et lecture de Wikipédia... 

J'imagine aussi beaucoup de projets, que j'aimerais réaliser mais qui seront sans doute impossibles par manque de temps : me filmer en train de jouer des morceaux et les poster sur PeerTube, écrire et illustrer une méthode de piano, jouer des morceaux très difficiles, utiliser le logiciel libre [LilyPond](http://lilypond.org/), qui permet de créer des partitions en vectoriel, à la manière de [LaTeX](https://www.latex-project.org/). 

J'espère pouvoir continuer ainsi encore un petit moment, on verra bien ce que cela donnera !

Tous les morceaux présents dans cet article proviennent du site [imslp.org](https://imslp.org/). Ils sont sous licence [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). La photo de courverture provient de [flickr.com](https://www.flickr.com/photos/32404172@N00/3391276983) et est sous licence [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/).

Merci de votre lecture, 

Brume
