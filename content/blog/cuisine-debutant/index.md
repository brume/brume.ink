+++
title = "Guide de débutant en cuisine"
date = 2021-05-15
description = "Conseils divers pour se diversifier et inaugurer de nouveaux plats."
[taxonomies]
categories = ["Cuisine"]
[extra]
month = "MAI"
illustration = "/blog/cuisine-debutant/cuisine-debutant.jpg"
+++

Vous en avez marre de manger des plats préparés ou des pâtes tous les jours ? Vous aimeriez bien vous diversifier, mais vous n'avez aucune idée de quoi préparer, vous prenez peur dès qu'il y a quelque chose qui cuit, ou vous n'avez aucune idée de quelle épice rajouter ? Ce guide est fait pour vous.

Vous l'aurez compris, nous allons parler de cuisine, et même plus spécifiquement de cuisine végétarienne (cependant, beaucoup de conseils sont généraux et peuvent s'appliquer à tout type de cuisine).

J'avais envie de parler de cuisine depuis quelque temps déjà, mais je ne savais pas vraiment par quoi commencer. Même si j'ai sous la main quelques recettes qui peuvent être sympathiques (ce sera pour une prochaine fois !), il en existe des millions sur internet, et quelques-unes de plus ne changeront probablement pas votre attrait pour la cuisine. Je vais donc m'atteler ici à quelque chose d'un peu plus général : des conseils divers pour débuter en cuisine, apprendre à composer des plats, se diversifier... 

En étant un·e étudiant·e qui cuisine végétarien, vous aurez probablement peu de temps, de matériel et de moyens, ni forcément l'envie et l'énergie de cuisiner plusieurs fois par jour, et encore moins de tester de nouveaux plats. Beaucoup de plats préparés contiennent de la viande, de même pour tout ce qui se cuisine très rapidement (jambon, saucisse...). C'est pourquoi souvent, on a tendance à se retrouver à manger principalement... des pâtes ou du riz.

Le but ici est d'apporter quelques solutions pour remédier à ce problème !

Toutes les photos présentes dans cet article sont issues de plats que j'ai cuisinés.

_Tofu frit, courgettes, edamames, nouilles chinoises et carottes crues._
![Tofu](tofu.jpg)

## La conception de plats

Vous allez devoir commencer par imaginer des plats. Au début, cela risque d'être compliqué ! Voici quelques pistes. 

### Lors des courses

Lorsque vous allez faire vos courses, vous pouvez tenter d'être curieux·se : balayez du regard les rayons, soyez attentif·ves. Essayez d'imaginer différents plats possibles avec les articles devant vous (cela marche surtout aux rayons des légumes, conserves, céréales...). Y a-t-il un produit que vous ne connaissez pas ? Notez le nom, vous pourrez faire des recherches dessus plus tard. 

Lorsque vous avez repéré un aliment, penser à une idée de plat qui vous plairait bien avec, vous pouvez rechercher sur internet comment réaliser cette recette, et acheter les ingrédients correspondants. 

Vous pouvez aussi essayer de ne pas utiliser de recette, et essayer de concevoir le plat seul·e. Ce guide a pour but de vous encourager à sortir des sentiers battus, d'être indépendant·e. Utiliser des recettes est toujours intéressant, pour découvrir de nouveaux plats par exemple, ou de nouvelles techniques, mais il est aussi bien d'acquérir un peu d'autonomie à ce niveau.

### Avec ce qu'il y a chez vous

_Aïe aïe aïe, cela fait plusieurs jours que ces courgettes traînent... elles commencent à se ratatiner. Il va donc falloir les cuisiner, mais que préparer avec ?_

Vous avez déjà ici un ingrédient clé de votre plat. Vous pouvez ensuite réfléchir à ce que vous avez d'autre qui irait bien avec, ou alors regarder des recettes sur internet.

Un site sympathique : [cuisinevg.fr](https://cuisinevg.fr/). Vous pouvez faire des recherches par ingrédients, consulter la liste, et vous dire : _"Ah, non, là, il me manque ça... ça c'est trop long à cuisiner... ça ça ne me fait pas envie... celle-là !"_

Si vous utilisez des recettes, ne soyez pas trop rigides. Vous pouvez totalement enlever un ingrédient, en rajouter un, enlever le superflu d'épices et herbes que vous ne possédez pas, changer les quantités... 

⚠️  Attention ! Ce conseil ne fonctionne pas pour la pâtisserie. Contrairement à la cuisine qui est assez approximative, la patisserie nécessite énormément de rigueur. Vous pouvez ne pas suivre la recette, mais votre taux de réussite sera bien amoindri ! 

### Avec vos souvenirs

Le meilleur moyen de cuisiner un nouveau plat, c'est d'utiliser vos souvenirs. Essayez de vous rappeler, vous avez forcément déjà vu des gens cuisiner autour de vous, vu des photos de plats, mangé une multitude de plats différents... Pensez à un plat qui vous semble à votre portée autant en termes d'accessibilité des ingrédients, que de temps et de matériel. 

Cette méthode est, par exemple, à combiner avec ce que vous avez chez vous. _"Quel plat ai-je déjà vu ou mangé avec des courgettes, qui me ferait envie maintenant ?"_

Vous pouvez aussi réfléchir avec des critères : _J'aimerais une soupe / un plat en sauce / quelque chose de frais / cuisiner rapidement..._

### Réflexion par composition

Une autre technique sera de réfléchir à votre plat par catégories :

Par exemple, vous souhaitez réaliser un plat équilibré, donc vous allez réfléchir par catégories d'aliment : 

- Céréales : pâtes, riz, boulgour, semoule, quinoa...
- Légumineuses : lentilles, lentilles corail, haricots rouges, pois cassés, pois chiches...
- Légumes : courgettes, poivrons, tomates, carottes...

Le but ici sera donc de choisir des éléments d'au moins 2 catégories. _"Je veux cuisiner mes courgettes, un légume. Je vais donc mettre avec une céréale... De la semoule !"_

_Exemple de salade à base de quinoa, oignons et poivrons crus, chèvre, herbes fraîches. Ici, on utilise deux catégories : des céréales et des légumes._
![Salade de quinoa](quinoa.jpg)

De manière générale, les plats en sauce (curry, couscous, chili...) passent bien en végétarien, car le plat est assez complet pour pas qu'il n'y ait l'impression "qu'il manque quelque chose". 

Si le côté "viande" vous manque trop, vous pouvez réaliser vos propres boulettes / steaks assez simplement en mixant (ou en écrasant à la fourchette) une légumineuse, avec un peu d'eau, de farine et des épices. Vous mélangez, donnez la forme souhaitée, vous les mettez à frire dans une poêle avec de l'huile, et le tour est joué !

_Exemple de steak de lentilles et patates sautées au paprika._
![Steak](steak.jpg)

### Catégories géographiques

Vous pouvez aussi réfléchir par catégorie géographique ! En effet, la cuisine française se concentre surtout autour de la viande ou du poisson, donc il est assez compliqué de cuisiner végétarien. C'est pourquoi il est souvent plus facile de piocher dans les autres cultures culinaires !

Voici quelques exemples : 

- Cuisine mexicaine : riz, haricots rouges, tomates, maïs, avocat, tortillas...
- Cuisine méditerranéenne : courgettes, aubergines, tomates, feta, olives ...
- Cuisine indienne : riz, légumineuses, pommes de terre, courgettes...
- Cuisine japonaise : riz, nouilles, avocat, œuf, tofu, edamame, algues...
- Cuisine française : haricots verts, pommes de terre, choux, petits pois, carottes, champignons...

Ces catégories n'ont rien d'officiel et sont réellement subjectives. Cependant, si vous voulez réaliser un plat d'une culture particulière, en vous servant dans l'une de ses catégories et en rajoutant les épices correspondantes (présentées plus bas), vous pourriez atteindre de bons résultats.

Vous pouvez totalement, et c'est même plus ou moins le but, essayer d'imaginer un plat qui est à la fois équilibré et à la fois issu d'une culture particulière !

_Chili sin carne, à base de riz, haricots rouges, tomates, oignons et coriandre fraîche. Cette combinaison d'ingrédients fera tout de suite un plat mexicain !_
![Chili](chili.jpg)

## La réalisation des plats

### Les ingrédients principaux nécessaires

Vous avez maintenant votre plat en tête. _Je suis bien avancé·e , mais comment le cuisiner maintenant ?_

Soit vous utilisez une recette, soit vous utilisez les catégories ci-dessus. Vous avez décidé, par exemple, de réaliser avec vos courgettes, des pâtes aux légumes, un peu à la méditerranéenne. 

Quels seront les ingrédients ? _"Bon, et bien déjà, des pâtes. Prenons des spaghettis. Ensuite, forcément, nos courgettes. Si l'on regarde dans la catégorie "méditerranéenne", qu'est-ce que l'on pourrait rajouter d'autre ? Eh bien, des tomates par exemple !"_

En général, quel que soit le plat choisi, si vous aimez cela, il est totalement possible de rajouter un oignon ou une gousse d'ail pour assaisonner. 

_"Bon, rajoutons donc un oignon, et voilà, j'ai la liste des ingrédients ! Spaghettis, courgettes, tomates, oignons."_

Un autre exemple ? Essayons de préparer un curry indien avec nos courgettes.
_Le curry se fait avec du riz en général, donc pour commencer, nous aurons besoin de riz. Ensuite, les légumes : les fameuses courgettes, des pommes de terre, et un oignon pour l'assaisonnement !_

Encore une fois, les catégories données au-dessus sont extrêmement approximatives, vous pourriez totalement rajouter des poivrons ou même des haricots verts à votre curry !

### Les étapes de réalisation

Bien, nous avons le plat, les ingrédients... mais comment le réaliser ?

Encore une fois, nous allons réfléchir un peu. Quel aspect aura notre plat ? Essayez de vous souvenir d'une photo du plat, ou mieux, de son goût et de sa consistance. Est-ce que c'est grillé, frit, cuit à la vapeur, à l'eau ? Est-ce que la sauce est liquide, épaisse, huileuse, onctueuse ?

Quelques questions à se poser concernant les étapes. 

### Casserole ou poêle ?

En sachant qu'ici on exclue le four pour des raisons de matériel, que choisir pour cuire entre une casserole ou une poêle ?
Ce que vous cuirez dans une casserole sera toujours de manière générale dans de l'eau ou dans une sauce. Par exemple, les céréales / légumineuses sont toujours à cuire séparément à la casserole dans de l'eau. Le temps de cuisson sera indiqué sur l'emballage. Il est également commun de mettre un peu de sel dans l'eau de cuisson des pâtes et autre. 

À moins d'avoir un souci de quantité, tout le reste peut se cuire à la poêle. L'idéal est d'avoir une sauteuse, une grande poêle aux bords relevés, qui permet de cuisiner en grande quantité / réaliser des plats en sauce sans risquer de déborder. 

### Cuisson à l'eau ou à l'huile ?

La cuisson à l'huile atteint des températures beaucoup plus élevées qu'à l'eau. Si vous atteignez de trop hautes températures, attention aux projections ! Elle est utilisée pour faire dorer, griller, frire des aliments. Dans le cas d'une friture, il y aura beaucoup d'huile, autrement, un filet suffit. 

Exemples : Patates sautées, boulettes, raviolis, légumes dorés...

La cuisson à l'eau est plus efficace pour les légumes, les céréales, et autre. Vous pouvez mettre un couvercle pour cuire plus vite mais attention en retirant le couvercle, la vapeur brûle ! Vous pouvez totalement avoir une poêle avec un fond d'eau comme votre casserole remplie d'eau, en fonction de si vous réalisez un plat avec une sauce, ou des aliments cuits à l'eau : pommes de terre, riz, pâtes, choux...

Ce que je fais souvent, dans le cas d'une sauce aux légumes par exemple, est un mélange des deux : je commence par dorer les légumes, puis je rajoute de l'eau ensuite pour constituer la sauce liquide et pour finir de cuire les légumes. 

Dans tous les cas, assurez-vous d'avoir toujours au moins de l'huile ou de l'eau dans votre poêle, sinon, vos aliments vont brûler ! N'hésitez pas à en rajouter si besoin. 

### Quelle puissance de feu utiliser ?

Le feu le plus fort sera utile pour bouillir de l'eau ou pour la friture. Pour cuire tout autre plat, on utilisera un feu moyen. Et enfin, pour les cuissons très lentes, ou pour réchauffer quelque chose, un feu faible. 

La puissance dépend aussi du type de plaque de cuisson : des feux à inductions chauffent énormément et très rapidement, tandis que des plaques électriques seront beaucoup plus lentes et moins puissantes. 

Il est assez difficile de passer d'un modèle à l'autre, cela demande un temps d'adaptation pour ne pas brûler ses plats ! De manière générale, lorsque vous utilisez des feux forts ou moyens, surveillez et remuez très régulièrement. 

Si vous avez l'impression que la situation vous dépasse (trop de choses cuisent en même temps, vous ne pouvez pas vous occuper de tout à la fois, quelque chose commence à brûler...), pas de panique : coupez les feux, déplacez vos poêles / casseroles si vous avez la place (les feux restent longtemps chauds après les avoir coupés) et reprenez petit à petit vos tâches une par une. 

### De retour à nos pâtes aux légumes... 

Il y aura donc les spaghettis, et les légumes en sauce. Les deux vont donc cuire séparément.

Les spaghettis cuiront dans une casserole avec de l'eau.

Ensuite, la sauce. Souvenons-nous de l'aspect de la sauce que nous aimerions obtenir. Ici, nous voudrions plutôt une sauce posée sur les pâtes, avec des petits morceaux, un peu huileuse, liquide, qui coule dans les pâtes. Nous devons donc couper les légumes en petits morceaux. Il n'y aura pas beaucoup de sauce liquide dans les légumes, donc nous pouvons utiliser une poêle. Nous cuirons tous les légumes ensemble, à l'eau, puis, pour obtenir l'aspect un peu huileux, ajouter un peu d'huile d'olive par la suite. Et voilà ! Nous avons toutes les étapes de cuisson. 

Prenons un autre exemple, notre curry. Nous voudrions que les légumes soient fondants, en gros morceaux, et qu'il y ait beaucoup de sauce, crémeuse.
Notre riz cuira dans une casserole à l'eau, tandis que nos légumes, comme nous aimerions beaucoup de sauce, cuiront également dans une casserole, d'abord avec de l'eau pour bien les cuire, puis avec, pourquoi pas, du lait de coco, pour obtenir le côté crémeux !

Voici quelques indications pour parvenir à vos fins. Vous voulez une sauce... 

- Épaisse, onctueuse : ajouter de la farine de blé ou de maïs (max 2 cuillères à soupes, attention !), de la crème.
- Crémeuse : lait (végétal ou non), crème.
- Huileuse : un peu d'huile. Attention, l'huile d'olive en particulier surpasse très vite le goût de tous les autres aliments !
- Liquide : ajouter de l'eau. 

### L'assaisonnement

Ça y est, votre plat est sur le feu. Les légumes cuisent, tout se passe bien. Mais pour l'instant, votre plat n'a pas beaucoup de goût... il faut rajouter des épices !

Le plus important ici est de goûter dès que vous ajoutez quelque chose. Goûtez entre chaque ajout votre plat en faisant attention à ne pas vous brûler. Ayez bien en tête le goût / l'odeur de chacune de vos épices. Au fur et à mesure, vous serez entrainés à les reconnaître dans un plat, et donc à le reproduire ! De plus, en goûtant régulièrement, vous apprendrez ce que chaque épice rajoute au plat, et vous pourrez décider à quel moment c'est suffisant. 

Voici une petite liste d'épices pour débuter :
- Ail : à ajouter au début de la cuisson, coupé finement. Ne pas mettre plus d'une ou deux gousses. Peut se manger cru (ex : pesto), mais sera très fort.
- Oignon : à ajouter au début de la cuisson, en général l'oignon entier. Peut se manger cru (ex : salade), mais sera très fort.
- Gingembre : en poudre ou entier (dans ce cas ajouter en début de cuisson), assez fort. Idéal pour les plats asiatiques.
- Citron : frais, à presser. Peut être utilisé dans beaucoup de plats mais change totalement la nature du plat.
- Thym : très passe-partout, s'accommode avec tout.
- Romarin : assez délicat à utiliser, car surplombe très vite un plat. Attention !
- Origan : passe bien avec les légumes aux herbes.
- Paprika : passe bien sur des œufs, des patates sautées, des sauces relevées...
- Cumin : attention, très fort en goût donc à utiliser toute petite quantité
- Cannelle : utile pour les desserts en tout genre, les fruits cuits... Difficile à utiliser avec du salé
- Curry : existe sous de multiples formats (poudres et pâtes), plus ou moins épicé.
- Sauce soja salée : utile pour des légumes marinés ou des soupes
- Sauce soja sucrée : utiles pour des plats sucrés, un peu "caramélisés". 
- Huile de sésame : très forte, surplombe facilement un plat. 
- Graines de sésame : donne un petit goût de noix aux plats, idéal pour le dressage ou en salade

Et voici la liste d'épices triée par catégorie géographique de plats que nous avons vu plus haut : 
- Mexicaine : piment, cumin, paprika...
- Méditerranéenne : huile d'olive, herbes de Provence, thym, origan, romarin...
- Indienne : curry, curcuma, cardamone, coriandre, massala, piment, lait de coco, paprika...
- Japonaise : sauce soja sucrée et salée, huile de sésame, graines de sésame, piment, citron, gingembre, mirin...
- Française : thym, herbes de Provence, feuilles de laurier, origan, romarin...

À cela, ajouter le sel et le poivre qui se glissent partout pour ajuster le goût d'un plat. 

Vous pensez avoir mis trop d'épices ? Vous pouvez ajouter de l'eau, qui diluera un peu le goût. Si c'est vraiment trop immangeable, vous pouvez rincer totalement vos aliments et recommencer la sauce / l'assaisonnement...
Votre plat est trop piquant ? Vous pouvez mettre du citron ou du lait / de la crème pour alléger. 

### Le service

Votre plat est fin prêt ! Mais il reste une dernière petite chose... comment le servir élégamment ? Ça n'a l'air de rien, et ce n'est absolument pas nécessaire, mais cela ajoute toujours une petite touche à votre plat, et cela fait réellement plaisir ! 

C'est assez simple. Il y a plein de petites choses que vous pouvez ajouter dans l'assiette, sur le plat, pour le rendre élégant : 
- des herbes fraîches : ciboulette, coriandre, menthe... assez compliqué à trouver (faites les pousser, ou trouvez-les au marché / dans les magasins bio ou asiatiques), mais cela fait toujours son effet !
- de la crème, la sauce que vous avez préparée,
- du poivre,
- des graines de sésame,
- des légumes coupés : une tomate, une carotte...

Soyez inventifs !

Vous pouvez maintenant déguster !

_Sobas, poivrons, edamames, oignons. Quelques herbes fraîches et graines de sésame donnent tout de suite bonne allure !_
![Sobas](sobas.jpg)


## Conseils divers 

- Ne vous découragez pas si vous avez raté votre plat, ça n'est pas très grave et vous apprendrez de vos erreurs. Plus vous cuisinerez, moins il y aura de ratés, mais sachez que cela arrive toujours de temps en temps !

- Ne culpabilisez pas de ne pas préparer de merveilleux et minutieux plats en permanence, il y aura toujours des jours où vos réserves sont vides, ou vous n'avez ni le temps ni l'énergie de cuisiner... 

- Vous pouvez essayer de préparer à manger en plus grandes quantités, pour avoir des restes pour les repas suivants. La plupart des plats peuvent se réchauffer sans micro-ondes, à la poêle, avec un peu d'eau pour les réhydrater.

- Vous pouvez totalement vous constituer au fur et à mesure une liste de plats, qui vous servira de réserve, et que vous pourrez utiliser tout le temps !

_Udon à l'œuf, avec tofu, algues, champignons, nouilles, sésame._
![Udon](udon.jpg)

À vos fourneaux !

Brume
