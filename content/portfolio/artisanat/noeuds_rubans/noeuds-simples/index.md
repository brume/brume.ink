+++
title = "Nœuds en rubans de satin"
description = ""
weight = 1
date = 2021-10-30
[taxonomies]
drawing-type = ["Artisanat"]
+++

Deux petits nœuds rouges
![Deux petits nœuds rouges](1.jpg)

Nœud rouge avec dentelle blanche
![Nœud rouge avec dentelle blanche](2.jpg)

Quatre épingles à petits nœuds roses et bleus
![Quatre épingles à petits nœuds roses et bleus](3.jpg)

Nœud bleu
![Nœud bleu](4.jpg)
