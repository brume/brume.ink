+++
title = "Autres accessoires kanzashi"
description = ""
weight = 4
date = 2021-10-30
[taxonomies]
drawing-type = ["Artisanat"]
+++

Composition bleue et noire avec chaînes, perles et fleurs
![Composition bleue et noire avec chaînes, perles et fleurs](1.jpg)

Gros nœud rouge avec une fleur au centre
![Gros nœud rouge avec une fleur au centre](2.jpg)

Quatre épingles avec petites fleur blanches
![Quatre épingles avec petites fleur blanches](3.jpg)

Broche fleurie de 14 fleurs et 2 glissières de pétals dans les tons bleus, 8h de travail
![Broche fleurie de 14 fleurs et 2 glissières de pétals dans les tons bleus](4.jpg)

