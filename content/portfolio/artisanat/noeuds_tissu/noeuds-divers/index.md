+++
title = "Nœuds aux motifs divers"
description = ""
weight = 2
date = 2021-10-30
[taxonomies]
drawing-type = ["Artisanat"]
+++

Deux nœuds rouges et gris à carreaux écossais
![Deux nœuds rouges et gris à carreaux écossais](1.jpg)

Nœud blanc à dentelle noire
![Nœud blanc à dentelle noire](2.jpg)
