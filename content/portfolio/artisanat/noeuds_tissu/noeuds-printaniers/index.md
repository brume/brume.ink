+++
title = "Nœuds printaniers"
description = ""
weight = 1
date = 2021-10-30
[taxonomies]
drawing-type = ["Artisanat"]
+++

Noeud blanc à fleurs rouges
![Nœud blanc à fleurs rouges](1.jpg)

Nœud bleu à fleurs blanches
![Nœud bleu à fleurs blanches](2.jpg)

Petit nœud rouge à fleurs
![Petit nœud rouge à fleurs](3.jpg)

Grand nœud bleu à fleurs
![Grand nœud bleu à fleurs](4.jpg)

Noeud rouge à fleurs jaunes
![Noeud rouge à fleurs jaunes](5.jpg)
