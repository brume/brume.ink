+++
title = "Association 42l"
description = "Association loi 1901 ayant pour mission de promouvoir la culture libre et l'éthique."
weight = 1
template = "projets/42l.html"
date = 2019-01-12 
[taxonomies]
project-name = ["42l"]
[extra]
illustration = "/projets/42l/42l-logo-bg.png"
+++
