+++
title = "Site de Guidi"
description = "Réalisation d'un site internet consacré aux œuvres de Guidi (1933-2001), peintre-sculpteur français." 
weight = 3
template = "projets/guidi.html"
date = 2021-12-12 
[taxonomies]
project-name = ["guidi"]
[extra]
illustration = "/projets/guidi/vignette.jpg"
+++
