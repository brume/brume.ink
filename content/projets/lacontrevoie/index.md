+++
title = "La Contre-Voie"
description = "L'Association 42l évolue ! Nouveau nom, nouveau site nouvelle charte graphique. Association d'intêrêt général qui défend l’éthique dans le numérique"
weight = 4
template = "projets/lacontrevoie.html"
date = 2022-09-25 
[taxonomies]
project-name = ["lacontrevoie"]
[extra]
illustration = "/projets/lacontrevoie/lcv-logo-bg.png"
+++
