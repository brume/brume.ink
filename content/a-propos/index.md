+++
title = "À propos"
template = "a-propos.html"
+++

Merci d'être de passage sur mon site ! 

Cet espace me permet de vous parler de mes gros [projets](/projets) réalisés, de rassembler mes dessins et autres créations dans le [portfolio](/portfolio#Croquis), ou encore un peu plus librement de ce qui m'intéresse dans le [blog](/blog). 

Puisse mon univers vous apporter un peu de rêverie, d'imagination ou encore d'inspiration !

Si vous désirez me contacter, ou en apprendre un peu plus sur moi, c'est [par ici](/contact). 

Si vous désirez être au courant des derniers articles ou illustrations, vous pouvez utiliser ce <a href="/rss.xml" download>flux RSS</a>.

Retrouvez ci-dessous quelques détails techniques : 

## Hébergement

[brume.ink](/) est hébergé chez PulseHeberg SAS.

Informations générales :
- Adresse : 9, Boulevard de Strasbourg, 83000 Toulon
- RCS Toulon 824 070 619
- Siret : 824 070 619 00039
- NAF : 63.11Z
- VAT : FR29824070619
- Téléphone : +33 (0) 4 22 14 13 60
- Email : contact @ pulseheberg.com

## Propriété intellectuelle

Sauf mention contraire, tout contenu publié sur ce site est sous licence [Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)](https://creativecommons.org/licenses/by-nc/4.0/), et le nom à créditer est Brume. 

Il peut m'arriver d'utiliser des contenus extérieurs en guise d'illustrations dans les projets ou les articles. La source de ces contenus sera toujours fournie sur la même page que le contenu en question.

## Le point technique

Ce site a été réalisé avec [Zola](https://www.getzola.org/), un générateur de site statique en Rust.

Le thème du site, ainsi que les templates, sont faits à la main ([Tera](https://tera.netlify.com/)/HTML pour le templating, et CSS). 

Par souci de respect de la vie privée, brume.ink ne fait aucune requête de tierce partie et ne possède aucune ligne de JavaScript. Aucune donnée n'est récoltée sur les visiteurs, excepté les journaux de connexion récupérés par PulseHeberg.

Les sources du site internet sont disponibles [ici](https://git.42l.fr/brume/brume.ink).
